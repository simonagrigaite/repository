<!DOCTYPE HTML>
<html>
<head>
    <title>Deadlink assignment</title>
     

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
         

    <style>
    .r-1em{ margin-right:1em; }
    .b-1em{ margin-bottom:1em; }
    .l-1em{ margin-left:1em; }
    .t0{ margin-top:0; }
    </style>
 
</head>
<body>
 

    <div class="container">
  
        <div class="page-header">
            <h1>Read Products</h1>
        </div>
     
        <?php

include 'database.php';
 
$action = isset($_GET['action']) ? $_GET['action'] : "";
 
if($action=='deleted'){
    echo "<div class='alert alert-success'>Record was deleted.</div>";
}
 
$query = "SELECT id, name, description FROM tenders ORDER BY id DESC";
$stmt = $con->prepare($query);
$stmt->execute();
 
$num = $stmt->rowCount();
 
echo "<a href='create.php' class='btn btn-success b-1em'>+Create New Product</a>";
 
//check if more than 0 record found
if($num>0){
 
    echo "<table class='table table-hover table-responsive table-bordered'>";
 
    echo "<tr>";
        echo "<th>ID</th>";
        echo "<th>Name</th>";
        echo "<th>Description</th>";
        echo "<th>Action</th>";
    echo "</tr>";
     

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){

    extract($row);
     
    // creating new table row per record
    echo "<tr>";
        echo "<td>{$id}</td>";
        echo "<td>{$name}</td>";
        echo "<td>{$description}</td>";
        echo "<td>";
            // read one record 
            echo "<a href='read.php?id={$id}' class='btn btn-warning'>Read</a>";
             
            echo "<a href='update.php?id={$id}' class='btn btn-primary r-1em'>Update</a>";
 
            echo "<a href='#' onclick='delete_user({$id});'  class='btn btn-danger'>Delete</a>";
        echo "</td>";
    echo "</tr>";
}
 
// end table
echo "</table>";
     
}
 
else{
    echo "<div class='alert alert-danger'>No records found.</div>";
}
?>
         
    </div>
     

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 
<script type='text/javascript'>

function delete_user( id ){
     
    var answer = confirm('Are you sure?');
    if (answer){
        window.location = 'delete.php?id=' + id;
    } 
}
</script>
 
</body>
</html>